@echo off
set PATH_TO_7ZIP=C:\"Program Files"\7-Zip\7z.exe
set PATH_ICUDTLDAT=nw\icudtl.dat
set PATH_NWPAK=nw\nw.pak
set PATH_NWEXE=nw\nw.exe
set PATH_PACKAGEJSON=package.json
set APP_NAME=DartDesktop
if not exist %PATH_ICUDTLDAT% (
	goto nwFileNotExist
)
if not exist %PATH_NWPAK% (
	goto nwFileNotExist
)
if not exist %PATH_NWEXE% (
	goto nwFileNotExist
)
if not exist %PATH_TO_7ZIP% (
	goto 7zipNotExist
)
goto build

:nwFileNotExist
echo Es fehlen Dateien im nw-Verzeichnis. 
echo Dort muessen sich folgende Dateien befinden: 
echo   icudtl.dat
echo   nw.pak
echo   nw.exe
echo Diese koennen von http://nwjs.io/ heruntergeladen werden.
goto eof

:7zipNotExist
echo Kann 7-Zip nicht finden. Bearbeite einfach diese Datei und setze den Pfad deiner 7-Zip Installation in der Variable 'PATH_TO_7ZIP'. 7-Zip kann hier heruntergeladen werden http://www.7-zip.de/ .

:build
call pub get
call pub build
%PATH_TO_7ZIP% a -tzip build\%APP_NAME%.nw .\build\web\* .\package.json
copy %PATH_ICUDTLDAT% build
copy %PATH_PACKAGEJSON% build
copy %PATH_NWPAK% build
copy /b %PATH_NWEXE%+build\%APP_NAME%.nw build\%APP_NAME%.exe
del /s build\%APP_NAME%.nw
del /s build\package.json

:eof