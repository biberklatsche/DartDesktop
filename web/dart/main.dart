import 'dart:html';
import 'dart:js';

void main() {
  SpanElement nameElem =  querySelector('#name');
  nameElem.text = 'Lars';
  ButtonElement closeButton = querySelector('#button-close');
  closeButton.onClick.listen((event) => window.close());
  window.onKeyDown.listen((KeyboardEvent e) {
    if (e.keyCode == KeyCode.ESC) {
      window.close();
      e.preventDefault();
    }
  });
}